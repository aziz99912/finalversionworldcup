import { AdminComponent } from './admin/admin.component';
import { ProfileComponent } from './profile/profile.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatchComponent } from './match/match.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VenteComponent } from './vente/vente.component';

const routes: Routes = [

  {path : "" , component : WelcomeComponent},
  {path : "Home" , component : HomeComponent},
  {path : "Match" , component : MatchComponent},
  {path:"Vente" , component : VenteComponent},
  {path : "Profile" , component : ProfileComponent},
  {path : "Admin" , component : AdminComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
