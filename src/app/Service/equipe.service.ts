import { Equipe } from './../models/equipe';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {
  apiUrl = "http://localhost:3000/api/equipe";


  constructor(public ps: EquipeService, public httpEquipe: HttpClient) { }

  getAllEquipe() {
    return this.httpEquipe.get<Equipe[]>(this.apiUrl)
  }
  getEquipeFid(id: number) {
    return this.httpEquipe.get<Equipe>(this.apiUrl + "/" + id);
  }
  deleteEquipe(id: number) {
    return this.httpEquipe.delete<any>(this.apiUrl + "/" + id);
  }
  ModifierEquipe(p: Equipe) {
    return this.httpEquipe.put<any>(this.apiUrl, p);
  }
  AjouterEquipe(p: Equipe) {
    return this.httpEquipe.post<any>(this.apiUrl, p);
  }
}

