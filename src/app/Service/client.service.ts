import { MAtchService } from './match.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  nom: string;
  pays: string;
  email: string;
  password: string;
  login: boolean;
  historique: number[];

  apiUrl = "http://localhost:8484/WorldCup/rest/user";



  constructor(public ps: MAtchService, public httpClient: HttpClient) {
    this.nom = "Karim";
    this.pays = "Tunisia";
    this.email = "karimghaith@gmail.com";
    this.password = "hello123";
    this.login = true;
    this.historique = [];
  }
  getAllClients() {
    return this.httpClient.get<Client[]>(this.apiUrl)
  }
  getClientFid(id: number) {
    return this.httpClient.get<Client>(this.apiUrl + "/" + id);
  }
  deleteClient(id: number) {
    return this.httpClient.delete<any>(this.apiUrl + "/" + id);
  }
  ModifierClient(p: Client) {
    return this.httpClient.put<any>(this.apiUrl, p);
  }
  AjouterClient(p: Client) {

    return this.httpClient.post<any>(this.apiUrl, p);
  }

  loginU(p){
   return this.httpClient.post(this.apiUrl+'/login',p)
  }

}
