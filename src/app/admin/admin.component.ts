import { Match } from './../models/match';
import { Client } from './../models/client';
import { Component, OnInit } from '@angular/core';
import { ClientService } from '../Service/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { WebRequestService } from '../Service/web-request.service';
import { MAtchService } from '../Service/match.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  match: Match[] | undefined;
  constructor(public ps: MAtchService, private router: Router, private WebRequest: WebRequestService) {

/*     this.ps.getAllMatche().subscribe(
      data => { this.LesClient = data; },
      error => { alert("erreur" + error); }

    ); */
  }

  delete(id: number) {
    this.ps.deleteMatche(id).subscribe(
      data => { alert("suppression avec succes") },
      error => { alert('suppression erroneé') }
    );
    this.router.navigateByUrl('/Admin');
  }

  modifier(id: number) {
    this.ps.ModifierMatche(id).subscribe(
      data => { alert("modification avec succes") },
      error => { alert('modification erroneé') }
    );
    this.router.navigateByUrl('/Admin');
  }
  ngOnInit(): void {
    this.WebRequest.get('user').subscribe((data) => {
      console.log('test')
      console.log(data)
    })

  }
}
